package org.example.controllers;

import org.example.dto.ProductDTO;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/products")
    public String productById(@RequestParam(value="id") Integer id, Model model) {
        ProductDTO result = productService.searchProductById(id);
        model.addAttribute("product", result);
        return "product";
    }

    @RequestMapping("/")
    public String greeting(Model model) {
        model.addAttribute("name", "World!");
        return "index";
    }


}
