package org.example.dto;

import java.util.List;

/**
 * Created by ctanglay on 11/22/15.
 */
public class ProductDTO {

    private int id;
    private String name;
    private List<FeatureDTO> featuresList;

    public ProductDTO(int id, String name, List<FeatureDTO> featuresList) {
        this.id = id;
        this.name = name;
        this.featuresList = featuresList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FeatureDTO> getFeaturesList() {
        return featuresList;
    }

    public void setFeaturesList(List<FeatureDTO> featuresList) {
        this.featuresList = featuresList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductDTO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", featureList=").append(featuresList);
        sb.append('}');
        return sb.toString();
    }
}
