package org.example.dto;

import org.example.models.FeatureDataType;

/**
 * Created by ctanglay on 11/22/15.
 */
public class FeatureDTO {

    private int id;
    private String name;
    private FeatureDataType dataType;

    public FeatureDTO(int id, String name, FeatureDataType dataType) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FeatureDataType getDataType() {
        return dataType;
    }

    public void setDataType(FeatureDataType dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FeatureDTO{");
        sb.append("name='").append(name).append('\'');
        sb.append(", dataType=").append(dataType);
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
