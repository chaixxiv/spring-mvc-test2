package org.example.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Product {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="Product_Feature",
        joinColumns = @JoinColumn(name = "Product_Id",
                                referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "Feature_Id",
                                referencedColumnName = "id"))
    private List<Feature> featuresList;

    public Product() {};

    public Product(String name, List<Feature> featuresList) {
        this.name = name;
        this.featuresList = featuresList;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Feature> getFeaturesList() {
        return featuresList;
    }

    public void setFeaturesList(List<Feature> featuresList) {
        this.featuresList = featuresList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Product{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", featuresList=").append(featuresList);
        sb.append('}');
        return sb.toString();
    }
}
