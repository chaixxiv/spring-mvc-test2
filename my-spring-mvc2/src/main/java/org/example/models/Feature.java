package org.example.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Feature {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String name;

    @Column(name="data_type")
    @Enumerated(value = EnumType.STRING)
    private FeatureDataType dataType;

    @Column
    @ManyToMany(mappedBy = "featuresList", fetch=FetchType.LAZY)
    private List<Product> productList;

    public Feature() {};

    public Feature(String name, FeatureDataType dataType) {
        this.name = name;
        this.dataType = dataType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public FeatureDataType getDataType() {
        return dataType;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Feature{");
        sb.append("name='").append(name).append('\'');
        sb.append(", dataType=").append(dataType);
        sb.append('}');
        return sb.toString();
    }
}
