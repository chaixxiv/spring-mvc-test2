package org.example.service;

import org.example.dto.ProductDTO;

/**
 * Created by ctanglay on 11/22/15.
 */
public interface ProductService {

   ProductDTO searchProductById(int productId);
}
