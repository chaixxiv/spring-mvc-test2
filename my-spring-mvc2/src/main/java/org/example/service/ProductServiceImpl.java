package org.example.service;

import org.example.dto.FeatureDTO;
import org.example.dto.ProductDTO;
import org.example.models.Product;
import org.example.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ctanglay on 11/22/15.
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public ProductDTO searchProductById(int productId) {

        Product product = productRepository.findOne(productId);
        if (product == null) {
            return null;
        }

        List<FeatureDTO> featureDTOList = product.getFeaturesList().stream().map(
                feature -> new FeatureDTO(feature.getId(), feature.getName(), feature.getDataType())
        ).collect(Collectors.toList());

        return new ProductDTO(product.getId(), product.getName(), featureDTOList);
    }


}
