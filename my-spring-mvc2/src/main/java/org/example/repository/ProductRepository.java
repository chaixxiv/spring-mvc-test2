package org.example.repository;

import org.example.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ctanglay on 11/22/15.
 */

public interface ProductRepository extends JpaRepository<Product, Integer> {


}
