INSERT INTO Product(id, name) VALUES(1, 'SONY Cybershot DSC-H200');
INSERT INTO Feature(id, data_type, name) VALUES(1, 'STRING', 'Cybershot H200 is DSLR-like quality camera');
INSERT INTO Feature(id, data_type, name) VALUES(2, 'NUMERICAL', '20.1');
INSERT INTO Feature(id, data_type, name) VALUES(3, 'STRING', 'http://image.priceprice.k-img.com/SONY_DSC_H200_L_1.jpg');
INSERT INTO Feature(id, data_type, name) VALUES(4, 'NUMERICAL', '7000.00');

INSERT INTO Product(id, name) VALUES(2, 'SONY Cybershot DSC-W800');
INSERT INTO Feature(id, data_type, name) VALUES(5, 'STRING', 'Cybershot DSC-W800 delivers the exceptional compact camera for any occasions.');
INSERT INTO Feature(id, data_type, name) VALUES(6, 'NUMERICAL', '20.4');
INSERT INTO Feature(id, data_type, name) VALUES(7, 'STRING', 'http://image.priceprice.k-img.com/SONY_DSC_W800_L_2.jpg');
INSERT INTO Feature(id, data_type, name) VALUES(8, 'NUMERICAL', '4,049.00');

INSERT INTO Product(id, name) VALUES(3, 'SONY Cybershot DSC-RX100');
INSERT INTO Feature(id, data_type, name) VALUES(9, 'STRING', 'High Quality, Crisp And Detailed Images.');
INSERT INTO Feature(id, data_type, name) VALUES(10, 'NUMERICAL', '20.2');
INSERT INTO Feature(id, data_type, name) VALUES(11, 'STRING', 'http://image.priceprice.k-img.com/SONY_DSC_W800_L_3.jpg');
INSERT INTO Feature(id, data_type, name) VALUES(12, 'NUMERICAL', '14,980.00');

INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (1,1);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (2,1);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (3,1);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (4,1);

INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (5,2);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (6,2);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (7,2);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (8,2);

INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (9,3);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (10,3);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (11,3);
INSERT INTO Product_Feature(Feature_Id, Product_Id) VALUES (12,3);